macroScript GCToonOff category:"GCScript"
(
	fn DxMaterialToMaterial obj=
	(
		if classof obj.material == DirectX_9_Shader then (			
			strStdDiffuseFilename = ""
			diffuseBitmap = obj.material.geteffectbitmap 2
			if diffuseBitmap as string != "BitMap:None" then (
				strDiffuseMapName =  diffuseBitmap.fileName
				extLength = strDiffuseMapName.count - 2
				strStdDiffuseFilename = replace strDiffuseMapName extLength 3 "psd"
				stdMaterialDiffuse = BitmapTexture fileName:strStdDiffuseFilename
				obj.material = standard diffuseMap:stdMaterialDiffuse showInViewport:true
			) else (		
				obj.material = standard showInViewport:true
			)
			
		)else if classof obj.material == Multimaterial do (			
			for i = 1 to obj.material.count do (				
				if classof obj.material[i] == DirectX_9_Shader do (
					strStdDiffuseFilename = ""
					diffuseBitmap = obj.material[i].geteffectbitmap 2
					if diffuseBitmap as string != "BitMap:None" then (			
						strDiffuseMapName =  diffuseBitmap.fileName
						extLength = strDiffuseMapName.count - 2
						strStdDiffuseFilename = replace strDiffuseMapName extLength 3 "psd"
						stdMaterialDiffuse = BitmapTexture fileName:strStdDiffuseFilename
						obj.material[i] = standard diffuseMap:stdMaterialDiffuse showInViewport:true
					) else (		
						obj.material[i] = standard showInViewport:true
					)
				)				
			)
		)	--end else if classof obj.material == Multimaterial do			
	)
	
	--main----------------------------------------------------------
	print "GCToonOff Start"
	for i in selection do (
		DxMaterialToMaterial  i
	)	
	redrawViews()
	print "\nGCToonOff Script end"
) 