-- (c) 2011 KOG Inc.
-- 2011-01-19
-- 메테리얼을 확인할 수 없을때 나던 에러 수정
-- 멀티 메테리얼일 경우 스탠다드 메테리얼로 바꾼다.
-- 게임에 맞도록 축을 맞춘다.
-- 선택한 오브젝트를(1이상) x파일로 익스포트한다


-- (c) 2001 KOG Inc.
-- Export objexts into MS DirectX X file(.x).
-- Notice : This program runs only if all object's coordinate system are Right-Handed(RHS).
-- Parameter : root object
-- Return : Xfile

-- (c) 2000 KOG(Physically-based Game Group)



-- for resetXFormEx(), MatrixRHS2LHS(), PointRHS2LHS(), and tab()
--include "util.ms"

-- decl. global variables


-- for resetXFormEx(), MatrixRHS2LHS(), PointRHS2LHS(), and tab()
--include "util.ms"

-- for indentation
macroScript GCExporter category:"GCScript" tooltip:"GCExporter - .X file로 익스포트 합니다."
(	

fn tab n = ( 
	s = ""
	for i = 1 to n by 1 do
		s = s + "    "
	return s
) -- fn tab
global g_sXFILEPATH = "d:\\CET\\recently\\"
-- decl. global variables
global g_outStream

-- for material
global g_nthObject
global g_sarObjectsMatName
global g_barUsedTexture

-- decl. functions
global xptXFile

-- get all descendents of a node(including itself).
-- DFS style
fn getDescendentsOf object = (
	local descendents = #()

	if object.children.count == 0 then 
		return #(object)

	append descendents object 
	for child in object.children do
		join descendents (getDescendentsOf child)

	return descendents
)


-- for setting texture coordiante.
fn _getTV2FaceList p3arDst p3arSrc = (
	ar = #()
		
	for i = 1 to p3arSrc.count by 1 do (
		n = p3arDst[i].x
		m = p3arDst[i].y
		k = p3arDst[i].z

		if ar[n] == undefined then (
			ar[n] = #()
			append ar[n] (p3arSrc[i].x as integer)
		) else append ar[n] (p3arSrc[i].x as integer)

		if ar[m] == undefined then (
			ar[m] = #()
			append ar[m] (p3arSrc[i].y as integer)
		) else append ar[m] (p3arSrc[i].y as integer)

		if ar[k] == undefined then (
			ar[k] = #()
			append ar[k] (p3arSrc[i].z as integer)
		) else append ar[k] (p3arSrc[i].z as integer)
	
	) -- for i
	
	return ar
) -- fn _getTV2FaceList()

fn _writeMaterial rootObject = (	
	local nObject, object

	descentObjects = (getDescendentsOf rootObject)	

	for nObject = 1 to descentObjects.count by 1 do (
		object = descentObjects[nObject]
		
		-- Whether the object is group's dummy
		if (classOf object) == Dummy then continue

		-- check material
		if object.material == undefined then continue
		
		--object.material.name = g_sMATPREFIX + "_" + object.name
		sCurObjMatName = object.material.name
				
		-- Eliminate space and '#' from the material's name
		-- and replace the characters with '_'.
		sarMatName = filterString sCurObjMatName " #"
		sMatName = ""
		for i = 1 to sarMatName.count by 1 do (
			if i != sarMatName.count then
				sMatName += sarMatName[i] + "_"
			else sMatName += sarMatName[i]
		) -- for i

		nFound = findItem g_sarObjectsMatName sMatName

		-- if the material isn't yet defined,
		if nFound == 0 then (
			g_sarObjectsMatName[nObject] = sMatName



			format "Material % {\n" sMatName to:g_outStream
			format "%" (tab 1) to:g_outStream
			format "// diffuse color(r, g, b and alpha)\n" to:g_outStream
			color = object.material.diffuse
			format "%" (tab 1) to:g_outStream
			format "%, %, %, %;;\n" \ 
				   (color.r/255.0) (color.g/255.0) (color.b/255.0) \
			       (object.material.opacity/100.0) to:g_outStream
			       -- (1.0) (1.0) (1.0) \
			       --(color.r/255.0) (color.g/255.0) (color.b/255.0) \ 
			       --(object.material.opacity/100.0) to:g_outStream
			
			format "%" (tab 1) to:g_outStream
			format "// specular power \n" to:g_outStream
			format "%" (tab 1) to:g_outStream
			format "%;\n" (20.0) to:g_outStream

			format "%" (tab 1) to:g_outStream
			format "// specular color(r, g and b)\n" to:g_outStream
			color = object.material.specular
			format "%" (tab 1) to:g_outStream
			format "%, %, %;;\n" (color.r/255.0) (color.g/255.0) (color.b/255.0) to:g_outStream

			format "%" (tab 1) to:g_outStream
			format "// emissive(self-Illumination) color(usually WHITE)\n" to:g_outStream
			format "%" (tab 1) to:g_outStream
			f = object.material.selfIllumAmount / 100.0
			format "%, %, %;;\n" (f) (f) (f) to:g_outStream



/*			
			format "Material % {\n" sMatName to:g_outStream
			format "%" (tab 1) to:g_outStream
			format "// diffuse color(r, g, b and opacity(alpha))\n" to:g_outStream
			color = object.material.diffuse
			format "%" (tab 1) to:g_outStream
			format "%, %, %, %;;\n" \ 
			       (1.0) (1.0) (1.0) \ 
			       (object.material.opacity/100.0) to:g_outStream
			       --(color.r/255.0) (color.g/255.0) (color.b/255.0) \ 
			       --(object.material.opacity/100.0) to:g_outStream
			
			format "%" (tab 1) to:g_outStream
			format "// specular power \n" to:g_outStream
			format "%" (tab 1) to:g_outStream
			format "%;\n" (20.0) to:g_outStream
			format "%" (tab 1) to:g_outStream
			format "// specular color(r, g and b)\n" to:g_outStream
			color = object.material.specular
			format "%" (tab 1) to:g_outStream
			format "%, %, %;;\n" (color.r/255.0) (color.g/255.0) (color.b/255.0) to:g_outStream
			format "%" (tab 1) to:g_outStream
			format "// emissive color(usually WHITE)\n" to:g_outStream
			format "%" (tab 1) to:g_outStream
			format "// Effect diffuse color.\n" to:g_outStream
			format "%" (tab 1) to:g_outStream
			f = object.material.selfIllumAmount / 100.0
			format "%, %, %;;\n" (f) (f) (f) to:g_outStream
*/			
			if  object.material.diffuseMap != undefined then (
				sDiffuseMap = object.material.diffuseMap.filename
				format "%" (tab 1) to:g_outStream
				format "TextureFilename {\n" to:g_outStream
				format "%" (tab 2) to:g_outStream
				format "\"%\";\n" (filenameFromPath sDiffuseMap) to:g_outStream
				format "%" (tab 1) to:g_outStream
				format "} // TextureFilename\n" to:g_outStream
				-- whether exporting texture coordinate or not
				g_barUsedTexture[nObject] = true	
			) else g_barUsedTexture[nObject] = false
			format "} // Material\n\n" to:g_outStream
		) else ( -- if nFound
			g_sarObjectsMatName[nObject] = g_sarObjectsMatName[nFound]
			g_barUsedTexture[nObject] = g_barUsedTexture[nFound]
		) -- else
	) -- for nObject

) -- fn _writeMaterial()

	
-- write Header
fn _writeHeader = (
	format "xof 0302txt 0064\n" to:g_outStream
	format "Header {\n" to:g_outStream
	format "%" (tab 1) to:g_outStream
	format "1;\n" to:g_outStream
	format "%" (tab 1) to:g_outStream
	format "0;\n" to:g_outStream
	format "%" (tab 1) to:g_outStream	
 	format "1;\n" to:g_outStream
	format "} // Header\n\n" to:g_outStream
) -- fn _writeHeader()


-- write Frame 
fn _writeFrame object nmObjectType = 
(
format "object = %" object 
print "end"

	format "\n" to:g_outStream
	format "Frame % {\n" ("x" + object.name) to:g_outStream
	format "%" (tab 1) to:g_outStream
	format "FrameTransformMatrix {\n" to:g_outStream

	-- if a transformation's determinantSign is positive, it is right-handed coordinate system.
	if object.transform.determinantSign > 0 then (
		nvCoordSys = #RHS
		format "OK   : %, %\n" object.name nvCoordSys
	) else (
		nvCoordSys = #LHS
		format "ERROR: %, %\n" object.name nvCoordSys
	)
	
	mR2L = (matrix3 [1,0,0] [0,0,1] [0,1,0] [0,0,0])
	if object.parent == undefined then (
		-- in case root node
		mTM = (object.transform)
	) else (
		mTM = (object.transform * (inverse object.parent.transform) )
	) -- if
	
	row = mTM.row1
	format "%" (tab 2) to:g_outStream
	format "%, %, %, 0.0,\n" row.x row.y row.z to:g_outStream

	row = mTM.row2
	format "%" (tab 2) to:g_outStream
	format "%, %, %, 0.0,\n" row.x row.y row.z to:g_outStream

	row = mTM.row3
	format "%" (tab 2) to:g_outStream
	format "%, %, %, 0.0,\n" row.x row.y row.z to:g_outStream

	row = mTM.row4
	format "%" (tab 2) to:g_outStream
	format "%, %, %, 1.0;;\n" row.x row.y row.z to:g_outStream

	
	format "%" (tab 1) to:g_outStream
	format "} // FrameTransformMatrix\n\n" to:g_outStream



	arFaceList = #()
	arTVList = #()
	
	for i = 1 to (getNumFaces object) by 1 do (
		-- format "%: % \n" i (getFace object i)
		append arFaceList (getFace object i)
	) -- for i
	
	for i = 1 to (getNumFaces object) by 1 do (
	--	format "i = %, object = %" i object 	
	--	print "end"
		getTVFace object i
		
		-- format "%: % \n" i (getTVFace object i)
		append arTVList (getTVFace object i)
		
		
	) -- for i
	

	
	
	
	TV2FaceList = _getTV2FaceList arTVList arFaceList

	-- Mesh
	--
	format "%" (tab 1) to:g_outStream
	format "Mesh % {\n" object.name to:g_outStream
	format "%" (tab 2) to:g_outStream
	format "%;\n" (getNumTVerts object) to:g_outStream

	-- Mesh vertices
	--
	nVert = getNumTVerts object
	for i = 1 to nVert by 1 do (
		-- vert = getVert object i
		try (
		nIndex = TV2FaceList[i][1]
		) catch ( nIndex = undefined )
		
		if nIndex != undefined then (
			-- Notice : in object.transform space
			vert = in coordsys (object.transform * mR2L) getVert object nIndex
		) else vert = [0, 0, 0]

		if i == nVert then ( -- last vertex
			format "%" (tab 2) to:g_outStream
			format "%;%;%;;\n\n" vert.x vert.y vert.z to: g_outStream
		) else ( 
			format "%" (tab 2) to:g_outStream
			format "%;%;%;,\n" vert.x vert.y vert.z to: g_outStream
		) -- else
	) -- for i
	
	-- Mesh faces
	--
	format "%" (tab 2) to:g_outStream
	format "%;\n" (getNumFaces object) to:g_outStream
	nFace = getNumFaces object
	for i = 1 to nFace by 1 do (
		face =  getTVFace object i

		format "%" (tab 2) to:g_outStream
		if i == nFace then ( -- the last face
			-- Notice : if the local coordinate system is RHS, flip the order of the vertices(DirectX : clockwise order).
			format "3;%,%,%;;\n\n" (face.x as integer - 1) \
                                   (face.z as integer - 1) \
		                           (face.y as integer - 1) to: g_outStream
		) else (
			format "3;%,%,%;,\n" (face.x as integer - 1) \
                                 (face.z as integer - 1) \
                                 (face.y as integer - 1) to: g_outStream
		) -- else

	) -- for i
	
	-- MeshMaterialList
	--
	format "%" (tab 2) to:g_outStream
	format "MeshMaterialList {\n" to:g_outStream
	format "%" (tab 3) to:g_outStream
	format "%;\n" (1) to:g_outStream
	nFaces = getNumFaces object
	format "%" (tab 3) to:g_outStream
	format "%;\n" (nFaces) to:g_outStream
	for i = 1 to nFaces by 1 do (
		if i != nFaces then (
			format "%" (tab 3) to:g_outStream
			format "%, \n" (0) to:g_outStream
		) else (
			format "%" (tab 3) to:g_outStream
			format "%; \n" (0) to:g_outStream
		) -- else
	) -- for i
	format "%" (tab 3) to:g_outStream
	format "{%}\n" g_sarObjectsMatName[g_nthObject] to:g_outStream
	format "%" (tab 2) to:g_outStream
	format "} // MeshMaterialList\n\n" to:g_outStream
	
	-- Mesh normals
	--
	format "%" (tab 2) to:g_outStream
	format "MeshNormals {\n" to: g_outStream
	format "%" (tab 3) to:g_outStream
	format "%;\n" (getNumTVerts object) to:g_outStream

	-- Vertex normals
	--
	nVert = getNumTVerts object

	for i = 1 to nVert by 1 do (
		try (
			nIndex = TV2FaceList[i][1]
		) catch ( nIndex = undefined )
		
		if nIndex != undefined then (
			-- Notice : in object.transform space
			vertNormal =  in coordSys (object.transform * mR2L) getNormal object nIndex
		) else vertNormal = [0, 0, 0]
		
		-- vertNormal =  getNormal object i
		if i == nVert then ( -- last vertex
			format "%" (tab 3) to:g_outStream
			format "%;%;%;;\n\n" vertNormal.x vertNormal.y vertNormal.z to:g_outStream
		) else (
			format "%" (tab 3) to:g_outStream	
			format "%;%;%;,\n" vertNormal.x vertNormal.y vertNormal.z to:g_outStream
		) -- else
	) -- for i
	
	-- Face normals
	--
	format "%" (tab 3) to:g_outStream
	format "%;\n" (getNumFaces object) to:g_outStream
	nFace = getNumFaces object
	for i = 1 to nFace by 1 do (
		face =  getTVFace object i

		format "%" (tab 3) to:g_outStream
		if i == nFace then ( -- the last face
			-- Notice : if the local coordinate system is RHS, flip the order of the vertices(DirectX : clockwise order).
			format "3;%,%,%;;\n\n" (face.x as integer - 1) \
                                   (face.z as integer - 1) \
		                           (face.y as integer - 1) to: g_outStream
		) else (
			format "3;%,%,%;,\n" (face.x as integer - 1) \
                                 (face.z as integer - 1) \
                                 (face.y as integer - 1) to: g_outStream
		) -- else
	) -- for i
	format "%" (tab 2) to:g_outStream
	format "} // MeshNormals\n\n" to: g_outStream
	
	if g_barUsedTexture[g_nthObject] == true then (
		-- Texture coordinations
		--
		format "%" (tab 2) to:g_outStream
		format "MeshTextureCoords {\n" to:g_outStream
		format "%" (tab 3) to:g_outStream
		format "%;\n" (getNumTVerts object) to:g_outStream
	
		nVert = getNumTVerts object
		for i = 1 to nVert by 1 do (
			vertTexture =  getTVert object i
			if i == nVert then ( -- last vertex
				format "%" (tab 3) to:g_outStream
				-- notice : flip y coordinate.
				format "%;%;;\n" (vertTexture.x) (1.0 - vertTexture.y) to: g_outStream
			) else ( 
				format "%" (tab 3) to:g_outStream
				format "%;%;,\n" (vertTexture.x) (1.0 - vertTexture.y) to: g_outStream
			) -- else
		) -- for i
	
		format "%" (tab 2) to:g_outStream	
		format "} // MeshTextureCoords\n\n" to: g_outStream
	) -- if g_barUsedTexture
	
	format "%" (tab 1) to:g_outStream	
	format "} // Mesh % \n\n" object.name to: g_outStream


	-- for recursion
	g_nthObject += 1
	
	children = object.children
	if children.count != 0 then (
		for i = 1 to children.count do (
			if classOf children[i] == Editable_mesh then 
			(
				-- Note: needs a specific naming.
				-- consider movable attribute
				if nmObjectType == #BUILDING then (
					if isOurObject children[i] == true then
						_writeFrame children[i] nmObjectType
				) 
				else -- #CAR
					_writeFrame children[i] nmObjectType
			) else
			(
				print "Warnning: not Editable_mesh in _writeFrame()"
			) -- else
		) -- for i
	)

	format "} // Frame % \n" object.name to: g_outStream

) -- fn _writeFrame()


-- main routine
fn xptXFile rootObject nmObjectType = 
(
	
	if classOf rootObject == Editable_mesh then 
	(
		-- initilize global variables. 
		-- for material
		-- indicate nth object
		g_nthObject = 1
		g_sarObjectsMatName = #()
		g_barUsedTexture = #()
		
	
		sFileName = g_sXFILEPATH + rootObject.name + ".x"
		-- sFileName = "d:\\temp\\" + rootObject.name + ".x"
		g_outStream = createFile sFileName
		
		disableSceneRedraw()
		
		m = rootObject.transform
		rootObject.transform = matrix3 1	
		-- X file Header
		--
		_writeHeader()
	
		-- Material
		--
		_writeMaterial rootObject
	
	
		-- Frame
		_writeFrame rootObject nmObjectType
	
		rootObject.transform = m	
	
		enableSceneRedraw()
	
	
		close g_outStream
	) else 
	(
		print "Error: not Editable_mesh in xptXFile()"
	) -- else
) -- fn xptXFile


--==========================



-- (c) 2011 KOG Inc.
-- 멀티 메테리얼일 경우 스탠다드 메테리얼로 바꿈
-- 리셋 엣스폼
-- x파일 익스포트
-- 사용법
-- 익스포트 하고자 하는 오브젝트들(다수의)을 선택해서 스크립트를 실행한다.
-- 오브젝트는 mesh와poly는 괜찮다.


-- (c) 2001 KOG Inc.
-- Export objexts into MS DirectX X file(.x).
-- Notice : This program runs only if all object's coordinate system are Right-Handed(RHS).
-- Parameter : root object
-- Return : Xfile

--include "XFile.ms"

	fn DetachPolygon_id multiMaterialObjArr =
	(	
		_debug = false

		if _debug do count = 0
		
		for i in multiMaterialObjArr do
		(
			if classof i.material == Multimaterial do
			(   
				i.name = "DeleteObject_GC"
				
	 			if _debug do format "i= %\n" i
				
				for j = 1 to i.material.materialList.count do
				(
					if _debug do count = count + 1
					if _debug do format "count = %\n" count
					local all_idsArr = #()
					if (getNumFaces i) > 1 do
					(
						for k = 1 to (getNumFaces i) do
						(
							try(
								if (findItem all_idsArr (polyOp.getFaceMatID i k)) == 0 do
								(
									append all_idsArr (polyOp.getFaceMatID i k)
								)
							)catch( if _debug do format "i = %, k = %\n" i k)
						)
					)
					if all_idsArr.count >0 do(
						
						local faceArr = #()
						temp_id = polyOp.getFaceMatID i 1 
						for j = 1 to (getNumFaces i) do
						(	
							try(
								if temp_id == polyOp.getFaceMatID i j do
								(
									append faceArr j
								)
							)catch( if _debug do format "i = %, k = %\n" i k)
						)
						if _debug do format "i = %, i.name = %\n" i i.material[temp_id].name
						polyOp.detachFaces i faceArr asnode:true name:(i.material[temp_id].name)					
						
					)
				)
			)		
		)	
		

		select $DeleteObject_GC*
		for i in selection do
		(
			try(
				if i.numverts == 0 do
				delete i
			)catch()
		)
	)
	
	

	
	fn ChangeMultiMaterialToStandard newObjectArr = 
	(		
		for i in newObjectArr do(
		--	print "class = "
		--	format "i = %, classof i = %" i (classof i)
			tmpID = polyOp.getFaceMatID i 1	
			i.material = i.material[tmpID]
		)		
	)

	fn InitArrayMultiMaterial multiMaterialObjArr selectArr=
	(
		for i in selectArr do
		( 
			mat = i.material
			print mat 
			if classof mat == Multimaterial do
			(
				append multiMaterialObjArr i
			)
		)
	)




	fn RotatePivotOnly obj rotation = 
	(
		local rotValInv=inverse (rotation as quat)	
		animate off in coordsys local obj.rotation *= RotValInv	
		obj.objectoffsetrot *= RotValInv	
		obj.objectoffsetpos *= RotValInv	
	)



	fn xptXFileTest rootObject nmObjectType =
	(
		format "rootObject% nmObjectType%" rootObject nmObjectType
		print "end"
	)
	--main-----------------------------------------------------------------------------------
	holdMaxFile() --안전하게 우선이므로 홀드(hold;저장)시킨후 페치(fetch;불러오기)한다
	
	multiMaterialObjArr = #()
	selectArr = #()
	newObjectArr = #()
	--newObjectArr = 스크립트가 실행된 후의 오브젝트(beforeObject) - 스크립트가 실행되기 전에 있던 오브젝트(afterObject)
	beforeObjectArr = #()
	afterObjectArr = #()
	
	
	if( selection.count == 0 ) then 
	(
		MessageBox "select export mesh" title:"GCScript"
	)else
	(
		holdMaxFile()
		for i in Objects do (
			append beforeObjectArr i
		)
		
		for i in selection do (
			append selectArr i
		)
		
		InitArrayMultiMaterial multiMaterialObjArr selectArr	
		DetachPolygon_id multiMaterialObjArr
		
		for i in Objects do (
			append afterObjectArr i
		)		
	
		--Init newObjectArr
		newObjectArr = afterObjectArr 
			
		for i = 1 to beforeObjectArr.count do(
			findIdx = findItem newObjectArr beforeObjectArr[i]
			if findIdx != 0 do(
				deleteItem newObjectArr findIdx
			)
		)
		--End newObjectArr 
			
		select newObjectArr
	
		ChangeMultiMaterialToStandard newObjectArr
		
		if findItem utilityplugin.classes Reset_XForm > 0 then (
			UtilityPanel.OpenUtility Reset_XForm
		)
		else (MessageBox "Utility does not seem to exist." title:"GCScript")
		

		exportArr = selectArr
		join exportArr newObjectArr
		for i = 1 to exportArr.count do(
		--	select newObjectArr[i]
			If ( isValidNode exportArr[i] ) do
			(	
				obj = selectArr[i]
				
				--메테리얼이 없으면 기본 메테리얼을 지정해서 툴에서 임포트 가능하도록 하기			
				if ( obj.material == undefined ) do 
					obj.material = standard showInViewport:true
			
	
				-- 오브젝트 축 맞추기			
				tempBox  = convertToPoly(Box())
				polyop.DeleteFaces tempBox #( 1,2,3,4,5,6 )
				
				tempBox.transform = matrix3 [1,-1.10467e-006,0] [0,-1.62921e-007,1] [1.10467e-006,1,1.62921e-007] [0,0,0]
				tempBox.name = obj.name
				
				tempAxisBox = Box()
				tempAxisBox.transform = matrix3 [1,-1.10467e-006,0] [0,-1.62921e-007,1] [1.10467e-006,1,1.62921e-007] [0,0,0]
				tempAxisBox.material = obj.material
	
				tempBox.attach obj	tempAxisBox
				convertToMesh(tempBox)
				tempBox.material = tempAxisBox.material
				
				try(
					xptXFile tempBox #CAR
				)catch()
				
			--	delete tempAxisBox -- 어차피 fetch됨
			)			
		)		
		fetchMaxFile quiet:true
	)
)
