macroScript GCToonToggle category:"GCScript"
(
	local isOpen = false --initialize to false (closed)

		on execute do
		(
			if isOpen then --if open, close it
			(				
				for o in objects do
				(
					--Off state
					macros.run "GCScript" "GCToonOff"
				)				
				isOpen = false --and lower the flag
			)
			else --if closed, open it
			(
				for o in objects do
				(
					--On state
					macros.run "GCScript" "GCToonOn"
				)
				isOpen = true --and raise the flag
			)
		)

	on isChecked return isOpen --return the flag
)
