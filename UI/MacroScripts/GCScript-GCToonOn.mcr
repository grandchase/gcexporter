macroScript GCToonOn category:"GCScript"
(
	/*
	1단계
	바로 적용한다
	(아무런 UI없음 -> 쉐이더파일, Cartoon Map이 지정된 경로에 있어야함)
	쉐이더 파일 위치 C:\Program Files\Autodesk\3dsMax8\maps\fx\GrandChase.fx
	Cartoon Map 위치 C:\Program Files\Autodesk\3dsMax8\maps\shell0vs.dds
	
	디퓨즈 파일 이름을 가져온다. 맵에 적용한다.
	2단계
	쉐이더 파일 위치, 기본 Cartoon Map 경로, Line Width, LineColor를 지정할 수 있는UI지원
	DiffuseMap Path를 정리혹은 기본 디퓨즈맵이 있는 경로를 지정할 수 있는 UI지원
	*/
	print "GCToonOn Start"
	
	shaderPath = "C:\Program Files\Autodesk\3dsMax8\maps\fx\GrandChase.fx"
	cartoonMapPath = "C:\Program Files\Autodesk\3dsMax8\maps\shell0vs.dds"
	
	--값을 복사해버려서 함수로 빼버릴 수 가 없다. = 배열어 넣더라도 값을 복사한다
	fn MaterialToDxMaterial obj=
	(
		if classof obj.material == Standardmaterial then (

			dxDiffuseMap = true;		
			if ( obj.material.diffuseMap == undefined ) then dxDiffuseMap = false
			else (			
				strDiffuseMapName = obj.material.diffuseMap.fileName
				extLength = strDiffuseMapName.count - 2
				strDXDiffuseMapName = replace strDiffuseMapName extLength 3 "dds"
			)			

			obj.material = DirectX_9_Shader()
			obj.material.effectfile = shaderPath
			
			dxCartoonBitmap = Bitmaptexture fileName:cartoonMapPath
			obj.material.seteffectbitmap 1 dxCartoonBitmap.bitmap
	
			if ( dxDiffuseMap ) do (
				dxDiffuseBitmap = Bitmaptexture fileName:strDXDiffuseMapName
				obj.material.seteffectbitmap 2 dxDiffuseBitmap.bitmap
			)		
		)else if classof obj.material == Multimaterial do (			
			for i = 1 to obj.material.count do (	
				dxDiffuseMap = true;		
				if ( obj.material[i].diffuseMap == undefined ) then dxDiffuseMap = false
				else (			
					strDiffuseMapName =  obj.material[i].diffuseMap.fileName
					extLength = strDiffuseMapName.count - 2
					strDXDiffuseMapName = replace strDiffuseMapName extLength 3 "dds"
				)
				
				print strDXDiffuseMapName
					
				obj.material[i] = DirectX_9_Shader()
				obj.material[i].effectfile = shaderPath
				
				dxCartoonBitmap = Bitmaptexture fileName:cartoonMapPath
				obj.material[i].seteffectbitmap 1 dxCartoonBitmap.bitmap
		
				if ( dxDiffuseMap ) do (
					dxDiffuseBitmap = Bitmaptexture fileName:strDXDiffuseMapName
					obj.material[i].seteffectbitmap 2 dxDiffuseBitmap.bitmap
				)
			)
		)	--end else if classof obj.material == Multimaterial do			
	)
	
	--main----------------------------------------------------------
	arrSelect = #()	
	for i in selection do (
		MaterialToDxMaterial i
	)
	redrawViews()
	print "\nGCToonView Script end"
	

)
