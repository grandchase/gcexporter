float3 g_LightDir : Direction 
<  
	string UIName = "Light Direction"; 
	string Object = "TargetLight";
	> = {-0.577, -0.577, 0.577};

float fOutLineWidth
<
	string UIName = "Line Width";
	string UIType = "Spinner";
	float UIMin = 0.0f;
	float UIMax = 10.0f;	
	float UIStep = 0.01;
> = 0.5f;

float4 g_vEdgeColor 
<
    string UIName = "Line Color";
> = {0.0f, 0.0f, 0.0f, 0.0f};
	
texture CartoonMap : CartoonTexture 
<
	string UIName = "Cartoon Map";
>;
	
texture DiffuseMap : DiffuseTexture 
< 
	string UIName = "Diffuse Map";	
>;

// transformations
float4x4 World			:		WORLD;
float4x4 View			:		VIEW;
float4x4 Projection		:		PROJECTION;
float4x4 WorldViewProj	:		WORLDVIEWPROJ;
float4x4 WorldView		:		WORLDVIEW;

sampler2D CartoonSampler = sampler_state
{
	Texture = <CartoonMap>;
	MinFilter = None;
	MagFilter = None;	
	AddressU = Wrap;
	AddressV = Wrap;
};

sampler2D DiffuseSampler = sampler_state
{
	Texture = <DiffuseMap>;
	MinFilter = None;
	MagFilter = None;	
	AddressU = Wrap;
	AddressV = Wrap;
};

struct VS_SKININPUT
{
    float4 vPos : POSITION;    
    float3 vNormal : NORMAL;
    float2 vTexCoord0 : TEXCOORD0; 	
};

struct VS_OUTPUT
{
    float4 Position		: POSITION;   // vertex position    
    float2 Tex0			: TEXCOORD0;  // vertex texture coords
    float2 Tex1			: TEXCOORD1;  // vertex texture coords
};

struct VS_OUTPUT2
{
    float4 Position		: POSITION;   // vertex position
    float2 Tex0			: TEXCOORD0;  // vertex texture coords
};

struct VS_OUTPUT_EDGE
{
    float4 Position		: POSITION;   // vertex position
    float4 Color		: COLOR;	  // vertex color
};


VS_OUTPUT RenderCartoonVS( VS_SKININPUT input )
{
    VS_OUTPUT Output;
    Output.Position = mul(input.vPos, WorldViewProj);        
    Output.Tex0 = input.vTexCoord0;    
    Output.Tex1.x = dot( normalize( mul(input.vNormal, World ) ) , normalize(g_LightDir) ) ;
    Output.Tex1.y = 0.0;
    return Output;
}

VS_OUTPUT_EDGE RenderBlackEdgeVS( VS_SKININPUT input )
{
    VS_OUTPUT_EDGE Output;       
    
	Output.Position = mul(input.vPos,WorldView);   
    
    Output.Position += normalize(mul(input.vNormal,WorldView)) * fOutLineWidth;        
	Output.Position = mul(Output.Position, Projection );
    Output.Color = g_vEdgeColor;
    return Output;
}
technique CartoonBlackEdge
{
    pass P0
    {
		Lighting		= false;
		AlphaBlendEnable = false;
		AlphaTestEnable = false;             
        ZEnable			= TRUE;
		ZWriteEnable	= TRUE;
        SrcBlend		= One;
        DestBlend		= Zero;
		// Stage0
		ColorOp[0]		= SelectArg1;
		ColorArg1[0]	= Texture;
		ColorArg2[0]	= Texture;
		// Stage1
        ColorOp[1]      = ADDSIGNED;
        ColorArg1[1]    = Current;
        ColorArg2[1]    = Texture;
        AddressU[1]     = Clamp;
        
        AlphaOp[0]		= Modulate;
        AlphaArg1[0]	= Texture;
        AlphaArg2[0]	= Diffuse;
		
		Texture[0] =<DiffuseMap>;
		Texture[1] = <CartoonMap>;
        VertexShader = compile vs_1_1 RenderCartoonVS();
        //PixelShader  = compile ps_2_0 RenderCartoonPS();
		PixelShader  = null;
    }  
	
	pass p1
    {
		Lighting			= false;    
		AlphaBlendEnable	= false;
		AlphaTestEnable		= false;
        SrcBlend			= One;
        DestBlend			= Zero;
        CullMode			= CCW; 
        ZEnable				= TRUE;
		ZFunc				= LESS;
		ZWriteEnable		= TRUE;	
		ColorOp[0]			= SelectArg1;
		ColorArg1[0]		= Diffuse;		
        		
        VertexShader = compile vs_1_1 RenderBlackEdgeVS();
		PixelShader  = null;
    }
}

technique CartoonNoEdge
{
    pass P0
    {
		Lighting		= false;
		AlphaBlendEnable = false;
		AlphaTestEnable = false;             
        ZEnable			= TRUE;
		ZWriteEnable	= TRUE;
        SrcBlend		= One;
        DestBlend		= Zero;
		// Stage0
		ColorOp[0]		= SelectArg1;
		ColorArg1[0]	= Texture;
		ColorArg2[0]	= Texture;
		// Stage1
        ColorOp[1]      = ADDSIGNED;
        ColorArg1[1]    = Current;
        ColorArg2[1]    = Texture;
        AddressU[1]     = Clamp;
        
        AlphaOp[0]		= Modulate;
        AlphaArg1[0]	= Texture;
        AlphaArg2[0]	= Diffuse;
		
		Texture[0] =<DiffuseMap>;
		Texture[1] = <CartoonMap>;
        VertexShader = compile vs_1_1 RenderCartoonVS();
        //PixelShader  = compile ps_2_0 RenderCartoonPS();
		PixelShader  = null;
    }  
}

technique NoCartoonNoEdge
{

	pass p0
    {
		Lighting			= FALSE;    
		AlphaBlendEnable	= FALSE;        
        ZEnable				= TRUE;
		ZWriteEnable		= True;
		Texture[0] =<DiffuseMap>;
		Texture[1] = <CartoonMap>;
		// Stage0
		ColorOp[0]			= SelectArg1;
		ColorArg1[0]		= Texture;
		
        VertexShader = null;
		PixelShader  = null;
    }    
}