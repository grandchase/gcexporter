------------------------------------------------------------
-----------------------------------------------      Library
-----------------------------------------------     by ������
----------------------------------------------------------------------

----------------------------------------------------------------Const
BaseMatrix = matrix3 [1,0,0][0,1,0][0,0,1][0,0,0]

----------------------------------------------------------------String Fnc
fn UpperCase instring =
(
	local upper, lower, outstring
	upper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	lower="abcdefghijklmnopqrstuvwxyz" 
	outstring = copy instring

	for i=1 to outstring.count do 
	(
		j=findString lower outstring[i] 
   		if (j != undefined) do outstring[i]=upper[j]
	)
	Return outstring
)
fn LowerCase instring =
(
	local upper, lower, outstring
	upper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	lower="abcdefghijklmnopqrstuvwxyz" 
	outstring = copy instring

	for i=1 to outstring.count do 
	(
		j=findString upper outstring[i] 
		if (j != undefined) do outstring[i]=lower[j]
	)
	Return outstring
)
fn TrimString SrcStr =
(
	local ResultStr = ""
	for i=1 to SrcStr.Count do
	(
		if SrcStr[i]!=" " then ResultStr = ResultStr + SrcStr[i]
	)
	return ResultStr
)
fn WriteStringByLen DstFile SrcStr StrLength =
(
	local NextSeekPos = (fTell DstFile)+StrLength
	WriteString DstFile SrcStr 
	for iStr = SrcStr.Count+2 to StrLength do WriteByte DstFile 0
	fSeek DstFile NextSeekPos #seek_set	
)
----------------------------------------------------------------Max Fnc
fn Matrix3ToFloat16 SrcMatrix =
(
	ResultList = #()
	ResultList[01] = SrcMatrix.Row1.x
	ResultList[03] = SrcMatrix.Row1.y
	ResultList[02] = SrcMatrix.Row1.z
	ResultList[04] = 0
	ResultList[09] = SrcMatrix.Row2.x
	ResultList[11] = SrcMatrix.Row2.y
	ResultList[10] = SrcMatrix.Row2.z
	ResultList[12] = 0
	ResultList[05] = SrcMatrix.Row3.x
	ResultList[07] = SrcMatrix.Row3.y
	ResultList[06] = SrcMatrix.Row3.z
	ResultList[08] = 0
	ResultList[13] = SrcMatrix.Row4.x
	ResultList[15] = SrcMatrix.Row4.y
	ResultList[14] = SrcMatrix.Row4.z
	ResultList[16] = 1
	Return ResultList
)
fn ColorToVector Color =
(
	Return [Color.r/255, Color.g/255, Color.b/255]
)
----------------------------------------------------------------File
fn WriteBoolean DstFile BoolValue =
(
	if BoolValue==True then	                  
	(
		WriteByte DstFile 1
	)
	else
	(
		WriteByte DstFile 0
	)
	Return True
)
fn WriteVector DstFile VectorValue =
(
	WriteFloat DstFile VectorValue.x
	WriteFloat DstFile VectorValue.z
	WriteFloat DstFile VectorValue.y
	Return True
)
fn WriteQuat DstFile QuatValue =
(
	WriteFloat DstFile QuatValue.x
	WriteFloat DstFile QuatValue.z
	WriteFloat DstFile QuatValue.y
	WriteFloat DstFile -QuatValue.w
	Return True
)
fn WriteMatrix3 DstFile MatValue =
(
	local Mat16 = Matrix3ToFloat16 MatValue
	for i = 1 to 16 do
	(
		WriteFloat DstFile Mat16[i]
	)
	Return True
)
fn CopyFromToFile DstFile SrcFile CopySize =
(
	if (DstFile!=Undefined)and(SrcFile!=Undefined)and(CopySize>0) then
	(
		local SizeDiv4 = CopySize/4 as Integer
		local SizeMod4 = (mod CopySize 4) as Integer
		for i = 0 to SizeDiv4-1 do
		(
			WriteLong DstFile (ReadLong SrcFile)
		)
		for i = 0 to SizeMod4-1 do
		(
			WriteByte DstFile (ReadByte SrcFile)
		)
		Return True
	)
	else
	(
		Return False
	)
)
----------------------------------------------------------------List
fn IsInList SrcObj SrcList =
(
	ReturnValue = False;
	for iObj in SrcList do
	(
		if iObj==SrcObj then ReturnValue = True
	)
	Return ReturnValue
)
fn GetIndexInList SrcObj SrcList =
(
	ReturnValue = 0;
	for iObj=1 to SrcList.Count do
	(
		if SrcList[iObj]==SrcObj then ReturnValue = iObj
	)
	Return ReturnValue
)
