-- @file   : GCExporter.ms
-- @author : 우동완 & 김윤정
-- @since  : 2006-03-03
-- @date   : 2006-03-17 modified and arranged by 박창현
--           2007-04-05 updated to 1.5 ver by 우동완

-- 1.5에 추가 된점.

-- 1. P3M을 뽑을때 Normal Vector가 잘 못 나오던것 수정. ☆
--     하지만 왠일인지 상황에 따라서 노말이 완전히 뒤집히는 경우가 발생한다.
-- 2. Export 된 P3M 파일의 용량이 필요 이상으로 크던 문제를 수정.
-- 3. Biped 이외에 Bone이 포함된 Mesh도 P3M으로 뽑을 쑤 있따! ☆☆☆
-- 4. Bone이 animation이 포함된 Motion도 FRM으로 뽑을 쑤 있따! ☆☆☆☆☆☆
--     단!!! Max 상에 존재하는 모든 Bone들의 이름은 Bone으로 시작해야 한다.

-- 같이 배포해야 할 파일. lib_fnc.ms 파일을 맥스/scripts 폴더에 넣어줘야 한다.

--------------------------------------------------------------------------------
-- common data & function
--------------------------------------------------------------------------------


Include "Lib_Fnc.ms"

global AUTO_SIZE_SCALE = 0.0045767944*1.1   -- auto size scale factor (GC캐릭터툴 참조)

-- 주어진 오브젝트의 로컬 변환을 얻는다.
fn GetLocalTM obj =
(
    matLocal = copy obj.transform

    if obj.parent != undefined then
    (
        matLocal = obj.transform * ( inverse obj.parent.transform )
    )

    return matLocal
)

--------------------------------------------------------------------------------
-- P3M Exporter
--------------------------------------------------------------------------------

global g_BoneTable = #()
global g_SelectedFaces = #()
global g_SkinVertices = #()
global g_MeshVertices = #()

struct MyBone
(
    name,
    m_objectID,
    m_BoneID,
    m_ParentObjectID,
    m_ParentBoneID,
    m_matBon,           -- Translation정보만
    m_matBonWorldPos,   -- Translation정보만
    Child = #()
)

struct Pos_Bone
(
    pos,
    child = #(),

    fn WriteFile fs =
    (
        WriteFloat fs (pos.x * AUTO_SIZE_SCALE)
        WriteFloat fs (pos.z * AUTO_SIZE_SCALE)
        WriteFloat fs (pos.y * AUTO_SIZE_SCALE)

        -- 원래 자식들의 개수는 10개이지만 4 byte align을 맞추기 위해 12로 한다.
        for i = 1 to 12 do
        (
            if child[i] == undefined then
            (
                WriteByte fs 255 #unsigned
            )
            else
            (
                WriteByte fs (child[i]-1) #unsigned
            )
        )
    )
)

struct Ang_Bone
(
    child = #(),

    fn WriteFile fs =
    (
        WriteFloat fs 0.0
        WriteFloat fs 0.0
        WriteFloat fs 0.0
        WriteFloat fs 0.0

        -- 원래 자식들의 개수는 10개이지만 4 byte align을 맞추기 위해 12로 한다.
        for i = 1 to 12 do
        (
            if child[i] == undefined then
            (
                WriteByte fs 255 #unsigned
            )
            else
            (
                WriteByte fs (child[i]-1) #unsigned
            )
        )
    )
)

struct SkinVertex
(
    pos, index, Nor, tu,

    fn WriteFile fs =
    (
        WriteFloat fs (pos.x * AUTO_SIZE_SCALE)
        WriteFloat fs (pos.z * AUTO_SIZE_SCALE)
        WriteFloat fs (pos.y * AUTO_SIZE_SCALE)

        WriteFloat fs 1.0
        WriteByte fs ( index - 1 )  #unsigned    -- 1-based index to 0-based index
        WriteByte fs ( index - 1 )  #unsigned    -- 1-based index to 0-based index
        WriteByte fs ( 0xff )       #unsigned
        WriteByte fs ( 0xff )       #unsigned

        WriteFloat fs (nor.x )
        WriteFloat fs (nor.z )
        WriteFloat fs (nor.y )

        WriteFloat fs (tu.x)
        WriteFloat fs (1.0 - tu.y)
    )
)

struct MeshVertex
(
    pos, nor, tu,

    fn WriteFile fs =
    (
        WriteFloat fs (pos.x * AUTO_SIZE_SCALE)
        WriteFloat fs (pos.z * AUTO_SIZE_SCALE)
        WriteFloat fs (pos.y * AUTO_SIZE_SCALE)

        WriteFloat fs (nor.x)
        WriteFloat fs (nor.z)
        WriteFloat fs (nor.y)

        WriteFloat fs (tu.x)
        WriteFloat fs (1 - tu.y)
    )
)

fn P3M_GetObjectIndex obj =
(
    for i = 1 to objects.count do
    (
        if objects[i] == obj then
            return i
    )
    return 0
)

fn P3M_GetBoneIndex bip_obj =
(
    index = 0
    for i = 1 to objects.count do
    (
        if classOf objects[i] == biped_object then
        (
            index += 1
            if objects[i] == bip_obj then
                return index
        )
        else if LowerCase (substring objects[i].name 1 4) == "bone" then
        (
            index += 1
            if objects[i] == bip_obj then
                return index
        )
    )
    return 0
)

fn SetBoneInfo obj_idx =
(
    r = MyBone()
    r.m_matBon = matrix3 1
    r.m_objectID = obj_idx
    r.m_boneID = P3M_GetBoneIndex( objects[obj_idx] )
    r.m_parentObjectID = P3M_GetObjectIndex( objects[obj_idx].parent )
    r.m_parentBoneID = P3M_GetBoneIndex( objects[obj_idx].parent )

    matWorldInvRot = objects[obj_idx].transform
    matWorldInvRot.row4 = point3 0 0 0
    matWorldInvRot = inverse(matWorldInvRot)

    matLocal = GetLocalTM objects[obj_idx]

    if objects[obj_idx].parent == undefined then
    (
        parentWorldRot = matrix3 1
    )
    else
    (
        parentWorldRot = objects[obj_idx].parent.transform
    )
    parentWorldRot.row4 = point3 0 0 0

    matFrm = matWorldInvRot * matLocal * parentWorldRot
    r.m_matBon.row4 = matFrm.row4

    r.m_matBonWorldPos = matrix3 0
    r.m_matBonWorldPos.row4 = objects[obj_idx].transform.row4

    return r
)

fn P3M_InitBoneTable =
(
    g_BoneTable = #()

    for k = 1 to objects.count do
    (
        if classOf objects[k] == biped_object then
        (
            append g_BoneTable (SetBoneInfo k)
        )
        if LowerCase (substring objects[k].name 1 4) == "bone" then
        (
            append g_BoneTable (SetBoneInfo k)
        )
    )
)

fn SaveBone fs =
(
    aPosBone = #()
    aAngBone = #()

    WriteByte fs g_BoneTable.count
    WriteByte fs g_BoneTable.count

    for x = 1 to g_BoneTable.count do
    (
        pBone = Pos_Bone()
        aBone = Ang_Bone()
        pBone.pos = g_BoneTable[x].m_matBon.row4

        append pBone.child x

        for child in objects[g_BoneTable[x].m_objectID].children do
        (
            c = P3M_GetBoneIndex child
            if c != 0 then
            (
                format "%번째 본 % 의 child %추가\n"x objects[g_BoneTable[x].m_objectID] child
                append aBone.child c
            )
        )
        append aPosBone pBone
        append aAngBone aBone
    )
    for posBone in aPosBone do
    (
        posBone.WriteFile fs
    )
    for angBone in aAngBone do
    (
        angBone.WriteFile fs
    )
)

-- for setting texture coordiante.
fn _getTV2FaceList p3arDst p3arSrc = (
  ar = #()

  for i = 1 to p3arSrc.count by 1 do (
    n = p3arDst[i].x
    m = p3arDst[i].y
    k = p3arDst[i].z

    if ar[n] == undefined then (
      ar[n] = #()
      append ar[n] (p3arSrc[i].x as integer)
    ) else append ar[n] (p3arSrc[i].x as integer)

    if ar[m] == undefined then (
      ar[m] = #()
      append ar[m] (p3arSrc[i].y as integer)
    ) else append ar[m] (p3arSrc[i].y as integer)

    if ar[k] == undefined then (
      ar[k] = #()
      append ar[k] (p3arSrc[i].z as integer)
    ) else append ar[k] (p3arSrc[i].z as integer)

  ) -- for i

  return ar
) -- fn _getTV2FaceList()


fn P3M_InitVertexTable =
(
    -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    -- < note : 피직 플러그인 > by park
    -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    -- 이 함수에서는 IPhysique.gup 플러그인의 인터페이스를 사용하게 된다.
    -- IPhysique.gup 파일은 stdplugs 폴더 안에 들어간다.
    -- physiqueOps.getPhysiqueModifier, physiqueOps.getVertexBone 함수에 대한 설명은
    -- IPhysique_Interface.doc 파일을 참조하기 바란다.
    -- IPhysique.gup 플러그인은 김용준 씨의 홈페이지인 "http://3DStudy.net"에서 구할 수 있다.
    -- 그리고 MAX 6.0용으로 컴파일된 IPhysique.gup를 사용하고 있다.
    -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

    -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    -- < note : 텍스처 좌표 얻기 > by park
    -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    -- MAX에서 texture의 u,v값을 얻기위해서는 texture vertex를 참조해야 한다.
    -- Direct3D에서는 각 vertex에 텍스처 좌표가 들어가는 식이지만,
    -- MAX에서는 '그냥 vertex'와 'texture vertex'를 구분해서 따로 사용하는 것으로 보인다.
    -- '그냥 vertex'는 정점의 위치(x,y,z)값을 담는 Point3 타입이고
    -- 'texture vertex'는 텍스처의 좌표(u,v,w)값을 담는 Point3 타입이다.
    -- '그냥 vertex'는 getVert 함수를 통해 얻을 수 있고,
    -- 'texture vertex'는 getTVert 함수를 통해 얻을 수 있다.
    -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    -- getFace 함수를 이용하면 해당 면을 구성하는 '그냥 vertex'의 인덱스를 알아낼 수 있다.
    -- 예를 들어, 첫번째, 두번째, 네번째 vertex로 구성된 면이라면 (1,2,4)로 된 Point3 타입의 값을
    -- 리턴하게 된다. 그러면, 1, 2, 4의 인덱스로 getVert 함수를 이용하여 '그냥 vertex'를 참조하면
    -- 그 면을 이루고 있는 각각의 정점에 대한 위치를 얻을 수 있다. 이와 유사하게 getTVFace 함수는
    -- 어떤 면을 이루는 점들의 'texture vertex'에 대한 인덱스를 얻는다. 예를 들어, (3,5,4)의 값을
    -- 받았다면 세번째, 다섯번째, 네번째 'texture vertex'의 인덱스를 의미한다. 그러면 세번째
    -- 'texture vertex'의 값을 getTVert 함수로 (u,v,w)를 얻으면 이는 그 면을 이루는 첫번째 정점의
    -- 텍스처 좌표가 되는 셈이다. 우리는 2차원 텍스처를 이용하므로 여기서 u,v만 의미있는 값이다.
    -- w값은 볼륨(3차원) 텍스처를 이용할 때나 의미가 있으므로 무시하면 된다.
    -- 참고로 Direct3D에서는 u,v의 원점 기준이 좌측 상단이지만, MAX에서는 u,v의 원점 기준이 좌측
    -- 하단이므로 v값을 익스포트할 때는 1.0-v로 거꾸로 바꿔줘야 한다.
    -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    mR2L = (matrix3 [1,0,0] [0,0,1] [0,1,0] [0,0,0])
    for o in selection do   -- 선택된 오브젝트에 대하여
    (
        -- 오브젝트의 피직 모디파이어를 얻어온다. 피직이 잡혀있지 않으면 에러가 난다.
        pm = physiqueOps.getPhysiqueModifier o

        -- 오브젝트로부터 메쉬를 얻어온다. (정점을 참조하기 위해서는 오브젝트가 아닌 메쉬여야 한다.)
        tmesh = snapshotAsMesh o
        format "\n텍스쳐 버텍스 개수 : %" (getNumTVerts tmesh)
        format "\n그냥 버텍스 개수 : %" (getNumVerts tmesh)
        format "\nface 개수 : %" (getNumFaces tmesh)

        g_SkinVertices = #()
        g_MeshVertices = #()


        arFaceList = #()
        arTVList = #()


        for i = 1 to (getNumFaces tmesh) by 1 do (
          -- format "%: % \n" i (getFace object i)
          append arFaceList (getFace tmesh i)
          ) -- for i

        for i = 1 to (getNumFaces tmesh) by 1 do (
          -- format "%: % \n" i (getTVFace object i)
          append arTVList (getTVFace tmesh i)
        ) -- for i

        TV2FaceList = _getTV2FaceList arTVList arFaceList

        nVert = getNumTVerts tmesh

        for i = 1 to nVert by 1 do
        (
          sknvertex = SkinVertex 0    -- 스킨 정점 생성
          mshVertex = MeshVertex 0    -- 메쉬 정점 생성

          try (
          nIndex = TV2FaceList[i][1]
          ) catch ( nIndex = undefined )

          if nIndex != undefined then (
            -- Notice : in object.transform space
            mshVertex.pos = getVert tmesh nIndex
          ) else mshVertex.pos = [0, 0, 0]

          sknvertex.index = P3M_GetBoneIndex (physiqueOps.getVertexBone o nIndex 1 modifier:pm)
          sknVertex.pos = sknvertex.pos = mshVertex.pos - (g_BoneTable[sknvertex.index].m_matBonWorldPos.row4)
          sknvertex.index = g_BoneTable.count + sknvertex.index

          if nIndex != undefined then (
            -- Notice : in object.transform space
            mshVertex.nor = getNormal tmesh nIndex

          sknVertex.nor = mshVertex.nor
          --sknVertex.nor = in coordsys (mR2L) mshVertex.nor

          ) else mshVertex.nor = [0, 0, 0]
          mshVertex.tu = getTVert tmesh i
          sknVertex.tu = copy mshVertex.tu


          -- 스킨 정점을 리스트에 추가한다.
          append g_SkinVertices sknvertex
          -- 메쉬 정점을 리스트에 추가한다.
          append g_MeshVertices mshVertex

        ) -- for i

        nVert = getNumVerts tmesh

         g_SelectedFaces = #()
         for i = 1 to tmesh.numfaces do
          (
              append g_SelectedFaces (getTVFace tmesh i)
          )



    )
)

fn SaveVertex fs =
(
    WriteShort fs g_SkinVertices.count
    WriteShort fs g_SelectedFaces.count

    for i = 1 to 260 do
    (
        WriteByte fs 0
    )

    -- MAX에서는 면을 정의할 때 반시계 방향으로 된 것을 앞면으로 보고
    -- Direct3D에서는 시계 방향으로 된 것을 앞면으로 본다.
    -- 그러므로 아래처럼 반시계 방향 -> 시계 방향 순서로 바꿔준다.
    for face in g_SelectedFaces do
    (
        WriteShort fs (face.x - 1)
        WriteShort fs (face.z - 1)
        WriteShort fs (face.y - 1)
    )

    for vert in g_SkinVertices do
    (
        vert.WriteFile fs
    )

    for vert in g_MeshVertices do
    (
        vert.WriteFile fs
    )
)

fn P3M_SaveFile strFileName =
(
    fstrm = fopen strFileName "wb"
    WriteString fstrm "Perfact 3D Model (Ver 0.5)"
    SaveBone fstrm
    SaveVertex fstrm
    fclose fstrm
)

--------------------------------------------------------------------------------
-- FRM Exporter
--------------------------------------------------------------------------------

-- 나도 창현이 형 처럼 주석 깔끔하게 달려고 했는데 막상 하려니까 귀찮다 -_-;
-- 창현님 존경 -_-b                   2007-04-05 우동완
global g_object_bone_table = #()
global g_AnimBoneTable = #()
global g_BipedTable = #()     -- Max상의 모든 Bone들을 모아놓은 Table. 1.5로 오면서 Biped 뿐만 아니라 "Bone"들도 같이 저장된다.
global g_DumpedBoneTable = #()
global g_GCBoneNameTable = #( "Pelvis", "L Thigh", "L Calf", "L Foot", "R Thigh", "R Calf", "R Foot", "Spine", "Head", "L UpperArm", "L Forearm", "L Hand", "R UpperArm", "R Forearm", "R Hand", "Bone01", "Bone02", "Bone03", "Bone04","Bone05","Bone06","Bone07" ,"Bone08" ,"Bone09" ,"Bone10" ,"Bone11" ,"Bone12" ,"Bone13" ,"Bone14" ,"Bone15" ,"Bone16" ,"Bone17" ,"Bone18" ,"Bone19" ,"Bone20" ,"Bone21" ,"Bone22" ,"Bone23" ,"Bone24" ,"Bone25","Bone26","Bone27","Bone28"  )

struct MyAnimBone
(
    name,
    m_objectID,
    m_BoneID,

    m_matWorldRot,
    m_ParentObjectID,
    m_ParentBoneID
)

fn ConvertNewBoneIndex2OldBoneIndex new_id =
(
    for i = 1 to g_BipedTable.count do
    (
        if ( findString g_BipedTable[i].name g_GCBoneNameTable[new_id] != undefined ) then
        (
            return i
        )
    )
    return undefined
)

fn GetObjectsByClass cl =
(
    varSetSameClass = #()

    for o in objects do
    (
        if classOf o == cl then
        (
            append varSetSameClass o
        )
    )
    varSetSameClass
)

fn FRM_InitBipedTable =
(
    g_BipedTable = #()

    for o in objects do
    (
        if classOf o == biped_object then
        (
            append g_BipedTable o
        )
        else if LowerCase (substring o.name 1 4) == "bone" then
        (
            append g_BipedTable o
        )
    )
)

fn FRM_InitObjBonTable =
(
    g_object_bone_table = #()

    id = 1
    for o in objects do
    (
        if classOf o == biped_object then
        (
            append g_object_bone_table id
            id += 1
        )
        else if LowerCase (substring o.name 1 4) == "bone" then
        (
            append g_object_bone_table id
            id += 1
        )
        else
        (
            append g_object_bone_table undefined
        )
    )
)

fn FRM_GetBoneIndex o_id =
(
    if o_id == undefined then
        return undefined

    return g_object_bone_table[o_id]
)

fn FRM_GetObjectIndex obj =
(
    for i = 1 to objects.count do
    (
        if obj == objects[i] then
        (
            return i
        )
    )
    return undefined
)

fn DumpObjects objclass =
(
    origbiped = #()
    snapset = #()

    for i in g_BipedTable do
    (
        append origbiped i
        b = snapshot i name:(i.name as string + "_snap")
        b.parent = undefined
        b.transform = i.transform
        b.position.track = bezier_position()
        b.rotation.track = bezier_rotation()
        append snapset b
    )

    for i = 1 to snapset.count do
    (
        try
        (
            snapset[i].parent = execute ("$'" + origbiped[i].parent.name + "_snap'")
        )
        catch
        (
            -- empty
        )
    )

    animate on
    undo off
    for t = animationRange.start to animationRange.end by 1 do at time t
    (
        for i = 1 to Snapset.count do
        (
            snapset[i].transform = origbiped[i].transform
        )
    )

    return snapset
)

fn FRM_SetBoneInfo obj_idx bon_idx =
(
    o_id = FRM_GetObjectIndex objects[obj_idx].parent
    b_id = FRM_GetBoneIndex o_id

    if b_id == undefined then
        b_id = 0
    if o_id == undefined then
        o_id = 0

    matWorldRot      = objects[obj_idx].transform
    matWorldRot.row4 = point3 0 0 0

    return MyAnimBone objects[obj_idx].name obj_idx bon_idx matWorldRot o_id b_id
)

fn Track iFrame boneID =
(
    p = point3 0 0 0
    s = point3 0 0 0
    matAni = matrix3 1
    multQuat = quat 0 0 0 1
        
    rotKey = g_DumpedBoneTable[boneID].rotation.controller.keys
--	  poskey = g_DumpedBoneTable[boneID].pos.controller.keys
--	  sclkey = g_DumpedBoneTable[boneID].scale.controller.keys

    multQuat = rotKey[iFrame].value * (inverse rotKey[1].value)
--    p = poskey[iFrame].value
--    s = sclkey[iFrame].value
--  multQuat = multQuat as quat
    multQuat.x = -multQuat.x
    multQuat.y = -multQuat.y
    multQuat.z = -multQuat.z

    rotate matAni multQuat
    
--	  translate matAni p
    
--	  scale matAni s
    
    return matAni
)

fn GetAnimMatForNewFRM iFrame bone_idx parentWorldRot =
(
    matFrm = matrix3 1

    matWorldRot      = g_DumpedBoneTable[bone_idx].transform
    matWorldRot.row4 = point3 0 0 0
    matWorldRotInv   = inverse matWorldRot

    matLocal = GetLocalTM g_DumpedBoneTable[bone_idx]

    if ( bone_idx == undefined ) then
    (
        bone_idx = 1
    )

    matAni     = Track iFrame bone_idx
    matTM      = matLocal * matAni
    matTM.row4 = matAni.row4

    matFrm      = matWorldRotInv * matTM *  parentWorldRot
    matFrm.row4 = point3 0 0 0

    return matFrm
)

fn GetAnimMatForPortableFRM iFrame bone_idx parentWorldRot =
(
    matFrm = matrix3 1

    matWorldRot      = g_BipedTable[bone_idx].transform
    matWorldRot.row4 = point3 0 0 0
    matWorldRotInv   = inverse matWorldRot

    matLocal = GetLocalTM g_BipedTable[bone_idx]

    matAni = Track iFrame bone_idx

    if ( ( (findString g_BipedTable[bone_idx].name "Pelvis") != undefined ) or
         ( (findString g_BipedTable[bone_idx].name "Spine") != undefined ) ) then
    (
        matAni2 = Track iFrame 1
        rotQuat = matAni2.rotationPart as Quat
        matAni2 = rotQuat as Matrix3
        matAni *= ( GetLocalTM g_BipedTable[1] ) * matAni2

        matTM       = matLocal * matAni
        matTM.row4  = matAni.row4
        matFrm      = matWorldRotInv * matTM
        matFrm.row4 = point3 0 0 0
    )
    else
    (
        matTM       = matLocal * matAni
        matTM.row4  = matAni.row4
        matFrm      = matWorldRotInv * matTM * parentWorldRot
        matFrm.row4 = point3 0 0 0
    )

    return matFrm
)

fn GetRootPos iFrame =
(
    return g_DumpedBoneTable[1].pos.keys[iFrame].value * AUTO_SIZE_SCALE
)

fn FRM_InitBoneTable =
(
    g_AnimBoneTable = #()
    o_index = 0
    b_index = 0
    for o in objects do
    (
        o_index = o_index + 1
        if classOf o == biped_object then
        (
            b_index = b_index + 1
            append g_AnimBoneTable (FRM_SetBoneInfo o_index b_index)
        )
        else if LowerCase (substring o.name 1 4) == "bone" then
        (
            b_index = b_index + 1
            append g_AnimBoneTable (FRM_SetBoneInfo o_index b_index)
        )
    )
)

fn FRM_SaveNewFile strFileName =
(
    fileStream = fopen strFilename "wb"

    -- 첫번째 프레임은 모델 참조용이므로 개수에서 하나 제외된다.
    WriteByte fileStream ((int)animationRange.end)  -- 애니메이션 프레임 개수를 기록한다.
    WriteByte fileStream g_DumpedBoneTable.count    -- 뼈대 개수를 기록한다.

    PrevRootPos = GetRootPos 1

    for iFrame = 2 to ((int)animationRange.end + 1) do
    (
        -- 옵션값은 일단 0으로 한다. 이 값은 GC캐릭터툴에서만 편집할 수 있다.
        WriteByte fileStream 0

        RootPos = GetRootPos iFrame

        PlusX = (RootPos.x - PrevRootPos.x)
        PosY = RootPos.z - (g_BipedTable[1].transform.row4.z * AUTO_SIZE_SCALE)

        PrevRootPos = RootPos

        WriteFloat fileStream PlusX -- PlusX 값을 기록한다.
        WriteFloat fileStream PosY  -- PosY 값을 기록한다.

        for i = 1 to g_DumpedBoneTable.count do
        (
            parentBoneID = g_AnimBoneTable[i].m_ParentBoneID
            if ( parentBoneID == 0 ) then
            (
                matWorldRot = matrix3 1
                matFRM = GetAnimMatForNewFRM iFrame i matWorldRot
            )
            else
            (
                matFRM = GetAnimMatForNewFRM iFrame i (g_AnimBoneTable[parentBoneID].m_matWorldRot)
            )

            WriteFloat fileStream matFRM.row1.x
            WriteFloat fileStream matFRM.row1.z
            WriteFloat fileStream matFRM.row1.y
            WriteFloat fileStream 0.0

            WriteFloat fileStream matFRM.row3.x
            WriteFloat fileStream matFRM.row3.z
            WriteFloat fileStream matFRM.row3.y
            WriteFloat fileStream 0.0

            WriteFloat fileStream matFRM.row2.x
            WriteFloat fileStream matFRM.row2.z
            WriteFloat fileStream matFRM.row2.y
            WriteFloat fileStream 0.0

            WriteFloat fileStream matFRM.row4.x
            WriteFloat fileStream matFRM.row4.z
            WriteFloat fileStream matFRM.row4.y
            WriteFloat fileStream 1.0
        )
    )
    PrevRootPos = GetRootPos 1
    for iFrame = 2 to ((int)animationRange.end + 1) do
    (
        RootPos = GetRootPos iFrame
        posZ = (RootPos.y - PrevRootPos.y)
        WriteFloat fileStream posZ
    )
    fclose fileStream
)
global g_PortableBoneCount = 1

fn GetPortableBoneCount =
(
	g_PortableBoneCount = g_GCBoneNameTable.count
	 for i = 1 to g_GCBoneNameTable.count do
   (
        bid = ConvertNewBoneIndex2OldBoneIndex i
        if bid == undefined then
        (
            g_PortableBoneCount = i - 1						
            exit
        )
   )
   
   
)


fn FRM_SavePortableFile strFileName =
(	
	GetPortableBoneCount()
    fileStream = fopen strFilename "wb"
    WriteByte fileStream ((int)animationRange.end)    
    WriteByte fileStream g_PortableBoneCount
    PrevRootPos = GetRootPos 1
    for iFrame = 2 to ((int)animationRange.end + 1) do
    (
        --옵션 , 플러스 액스 , 포스 와이 , 값은 일단 0으로 한다,
        WriteByte fileStream 0
        RootPos = GetRootPos iFrame

        PlusX = (RootPos.x - PrevRootPos.x)
        PosY = RootPos.z - (g_BipedTable[1].transform.row4.z * AUTO_SIZE_SCALE)

        PrevRootPos = RootPos
        WriteFloat fileStream PlusX
        WriteFloat fileStream PosY

        for i = 1 to g_GCBoneNameTable.count do
        (
            b_id = ConvertNewBoneIndex2OldBoneIndex i
            if b_id == undefined then
            (
                exit
            )

            parentBoneID = g_AnimBoneTable[b_id].m_ParentBoneID

            matFRM = GetAnimMatForPortableFRM iFrame b_id (g_AnimBoneTable[parentBoneID].m_matWorldRot)

            WriteFloat fileStream matFRM.row1.x
            WriteFloat fileStream matFRM.row1.z
            WriteFloat fileStream matFRM.row1.y
            WriteFloat fileStream 0.0

            WriteFloat fileStream matFRM.row3.x
            WriteFloat fileStream matFRM.row3.z
            WriteFloat fileStream matFRM.row3.y
            WriteFloat fileStream 0.0

            WriteFloat fileStream matFRM.row2.x
            WriteFloat fileStream matFRM.row2.z
            WriteFloat fileStream matFRM.row2.y
            WriteFloat fileStream 0.0

            WriteFloat fileStream matFRM.row4.x
            WriteFloat fileStream matFRM.row4.z
            WriteFloat fileStream matFRM.row4.y
            WriteFloat fileStream 1.0
        )
    )
PrevRootPos = GetRootPos 1
    for iFrame = 2 to ((int)animationRange.end + 1) do
    (
        RootPos = GetRootPos iFrame
        posZ = (RootPos.y - PrevRootPos.y)
        WriteFloat fileStream posZ
    )

    fclose fileStream
)

--------------------------------------------------------------------------------
-- Export functions
--------------------------------------------------------------------------------

fn ExportP3MNew =
(
    if selection.count != 1 then
    (
        messageBox "익스포트하고 싶은 오브젝트를 딱 한 개만 선택해 주세요."

        return false
    )

    if ( strFileName = getSaveFileName filename:"default.p3m" types:"P3M(*.p3m)|*.p3m|All|*.*|" ) != undefined then
    (
        P3M_InitBoneTable()
        P3M_InitVertexTable()

        P3M_SaveFile strFileName

        messageBox "P3M(몬스터) 생성 완료."

        return true
    )

    return false
)

fn ExportFRMNew =
(
    if ( strFileName = getSaveFileName filename:"default.frm" types:"FRM(*.frm)|*.frm|All|*.*|" ) != undefined then
    (
        sliderTime = 0f -- 슬라이드의 프레임을 0번째로 강제로 옮긴다.

        FRM_InitObjBonTable()
        FRM_InitBipedTable()
        FRM_InitBoneTable()

        g_DumpedBoneTable = DumpObjects biped_object
        FRM_SaveNewFile strFileName
        delete g_DumpedBoneTable

        messageBox "FRM(몬스터) 생성 완료."

        return true
    )

    return false
)

fn ExportFRMPortable =
(
    if ( strFileName = getSaveFileName filename:"default.frm" types:"FRM(*.frm)|*.frm|All|*.*|" ) != undefined then
    (
        sliderTime = 0f -- 슬라이드의 프레임을 0번째로 강제로 옮긴다.

        FRM_InitObjBonTable()
        FRM_InitBipedTable()
        FRM_InitBoneTable()

        g_DumpedBoneTable = DumpObjects biped_object
        FRM_SavePortableFile strFileName
        delete g_DumpedBoneTable

        messageBox "FRM(캐릭터) 생성 완료."

        return true
    )

    return false
)

--------------------------------------------------------------------------------
-- utility definition
--------------------------------------------------------------------------------

utility GCExporter "GC Exporter ver 1.5"
(
    -- 그림 파일
    bitmap bmp fileName:"GCExporter.bmp"

    -- 버튼 정의
    button btnP3MNew      "P3M(몬스터)" align:#center width:120 tooltip:"새로운 모델 구조의 P3M 모델을 생성합니다."
    button btnFRMNew      "FRM(몬스터)" align:#center width:120 tooltip:"새로운 모델 구조가 적용된 FRM 모션을 생성합니다."
    button btnFRMPortable "FRM(캐릭터)" align:#center width:120 tooltip:"이전 모델 구조와 호환되는 FRM 모션을 생성합니다."

    -- 'P3M(몬스터)' 버튼을 눌렀을 때
    on btnP3MNew pressed do
    (
        ExportP3MNew()
    )

    -- 'FRM(몬스터)' 버튼을 눌렀을 때
    on btnFRMNew pressed do
    (
        ExportFRMNew()
    )

    -- 'FRM(캐릭터)' 버튼을 눌렀을 때
    on btnFRMPortable pressed do
    (
        ExportFRMPortable()
    )

)